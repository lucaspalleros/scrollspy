console.log('Loaded');

let activeClass = 'azul';
let hasChange = false;

const next = async () => {
    console.log('next');
    setTimeout( () => {
        document.querySelector(`#${activeClass}`).scrollIntoView({scroll:'smooth'})
        hasChange = false;
    }, 900);
}

const callback = async (entries1) => {
    const entries = entries1;
    entries.forEach( ({isIntersecting,target}) => {
        let className = target.classList[1]
        if(isIntersecting && className !== activeClass) {
            activeClass = className;
            let items = document.querySelectorAll('.list-item')
            items.forEach(el => el.classList.remove("active"))    
            document.querySelector(`#${className}`).classList.add('active'); 
            hasChange = true;
        } 
    });
    if (hasChange) {
      next()
    };
};

const options = {
    threshold: 1,
}

const observer = new IntersectionObserver(callback,options);

const boxes = document.querySelectorAll('.box'); 

boxes.forEach(el => observer.observe(el));

document.querySelector('.scroll').addEventListener('click', ({target}) => {
    console.log(target.classList.value);
    if(target.classList.value !== 'scroll'){
        window.location.hash = `#${target.id}-box`;
    }
});


// document.querySelector(`#last`).scrollIntoView()